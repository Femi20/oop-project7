#ifndef SIZE_H
#define SIZE_H
#include "history.h" 

class Size:  public History
{

public:

  Size(const int w, const int h);
  void setWeight(const int w);
  void setHeight(const int h);
  void SayHello(); 
  int  getWeight();
  int  getHeight(); 
private:

  int Height;
  int Weight; 
  
};


#endif 
