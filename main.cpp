#include <iostream>
#include "size.h"
#include "extra.h"

using namespace std;

int main(void)

{
  Size S(89,200);
  Extra E(1);

  S.SayHello();
  E.SayHello();
  cout <<" And finally... Hello from main" << endl; 
  return 0;

} 
