# Femi Adeyemo
# COP 3353
# Assignment 7
# 04/12/2017

CC = g++
CFLAGS = -c -g -I.
LFLAGS = -g

main: extra.o size.o history.o main.o  
	$(CC) $(LFLAGS) -o main extra.o size.o history.o main.o #Liks objects files to main routine		
	

extra.o: extra.cpp extra.h
	$(CC) $(CFLAGS) $(LFLAGS) extra.cpp #Compiles extra.cpp file


size.o: size.cpp size.h
	$(CC) $(CFLAGS) $(LFLAGS) size.cpp #Compiles size.cpp file
	

history.o: history.cpp history.h
	$(CC) $(CFLAGS) $(LFLAGS) history.cpp #Compiles history.cpp file
	

main.o: main.cpp size.h extra.h
	$(CC) $(CFLAGS) $(LFLAGS) main.cpp #Compiles main.cpp file
 

clean: 
	rm *.o main
